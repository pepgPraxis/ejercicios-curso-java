SECCIÓN 1

1. Un lenguaje DDL es el encargado de definir la estructura de datos
2. El nivel READ UNCOMITTED consiste en peritir leer datos que no han sido comprometidos.
3. La integridad referencial es la manera de asegurarse que los datos sean válidos.
4. Crear un índice nos ayuda a realizar las busquedas más rapidamente.
5. BEGIN - Inicia la transacción.
   COMMIT - Compromete la información.
   ROLLBACK - Deshace la informmación hasta el último punto de guardado si algo sale mal.
   SAVEPOINT - Define los puntos a los que se puede regresar si algo sale mal.
6. La diferencia entre UNION y JOIN es que el primero hace la una tabla con todas las columnas, mientras que JOIN hace la combinación de las columnas.
7. La clausula CASE se utiliza como un IF en el que se comparan 2 o mas valores 
CASE WHEN calificacion > 7 THEN 'Aprobado' ELSE 'Reprobado' END;



SECCIÓN 2

1.
CREATE TABLE alumno(
id_alumno INT PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
num_cuenta VARCHAR(20) NOT NULL UNIQUE,
licenciatura VARCHAR(15)
);

2.
CREATE TABLE profesor(
id_profesor INT PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
num_empleado VARCHAR(10) NOT NULL UNIQUE,
fecha_ingreso DATE NOT NULL,
fecha_retiro DATE,
CONSTRAINT CK_fecha_ingreso CHECK (fecha_ingreso<= GETDATE()),
CONSTRAINT CK_fecha_retiro CHECK (fecha_retiro<= GETDATE())
);

3.
CREATE TABLE materia(
id_materia INT PRIMARY KEY,
nombre_materia VARCHAR(30) NOT NULL
);

4.
CREATE TABLE calificacion(
id_materia INT,
id_alumno INT,
id_profesor INT,
calificacion NUMERIC,
FOREIGN KEY (id_materia) REFERENCES materia(id_materia),
FOREIGN KEY (id_alumno) REFERENCES alumno(id_alumno),
FOREIGN KEY (id_profesor) REFERENCES profesor(id_profesor),
PRIMARY KEY (id_materia,id_alumno),
CONSTRAINT CK_Cal_Calificacion CHECK (calificacion>=0 and calificacion <=10)
);

5.
SELECT a.nombre, AVG(calificacion) AS promedio
FROM alumno a INNER JOIN calificacion c 
ON a.id_alumno=c.id_alumno
GROUP BY a.nombre

6.
SELECT m.nombre_materia
FROM materia m INNER JOIN calificacion c
ON m.id_materia=c.id_materia
WHERE c.calificacion<7
GROUP BY m.nombre_materia
ORDER BY m.nombre_materia DESC LIMIT 1

7.
SELECT p.nombre
FROM profesor p JOIN calificacion c 
ON (p.id_profesor = c.id_profesor)
WHERE c.calificacion < 7
GROUP BY p.nombre
ORDER BY COUNT(*) desc LIMIT 1

8.
SELECT a.nombre
FROM alumno a LEFT JOIN calificacion c 
ON a.id_alumno = c.id_alumno
WHERE c.calificacion IS NULL
GROUP BY a.nombre

9.
SELECT * 
FROM profesor
WHERE fecha_retiro IS NOT NULL;

10.
SELECT *
FROM materia
WHERE id_materia IS NULL

11.
FROM profesor
WHERE EXTRACT(YEAR FROM fecha_ingreso) < 2009
